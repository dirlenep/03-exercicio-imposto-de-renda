import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * @author Dirlene Preilipper
 * @version 1.0
 */
public class ImpostoRenda {

	/**
	 * C�lculo de Sal�rio L�quido e Desconto do Imposto de Renda
	 * @param salario entrada do sal�rio bruto 
	 * @return sa�da do sal�rio l�quido com desconto do imposto
	 */
	public double calcImposto(double salario) {
		// TODO Auto-generated method stub
		double salarioLiquido = 0;
		double imposto = 0;
		double aliquota7 = 69.20;
		double aliquota15 = 138.66;
		double aliquota22 = 205.57;
		
		if (salario <= 1903.98) {
			salarioLiquido = salario;
		} else if (salario >= 1903.99 && salario <= 2826.65) {
			imposto = (salario - 1903.98) * 0.075;
			salarioLiquido = salario - imposto;
		} else if (salario >= 2826.66 && salario <= 3751.05) {
			imposto = ((salario - 2826.65) * 0.15) + aliquota7;
			salarioLiquido = salario - imposto;
		} else if (salario >= 3751.06 && salario <= 4664.68) {
            imposto = ((salario - 3751.05) * 0.225) + aliquota7 + aliquota15;
			salarioLiquido = salario - imposto;
		} else if (salario > 4664.68) {
            imposto = ((salario - 4664.68) * 0.275) + aliquota7 + aliquota15 + aliquota22;
			salarioLiquido = salario - imposto;
		}
		
	    BigDecimal OutputSalario = new BigDecimal(salarioLiquido).setScale(2, RoundingMode.HALF_EVEN);
	    BigDecimal OutputImposto = new BigDecimal(imposto).setScale(2, RoundingMode.HALF_EVEN);
		System.out.println("Sal�rio L�quido: " + OutputSalario + " - IRPF: " + OutputImposto);
		return salarioLiquido;
	}

}
