import static org.junit.Assert.*;
import org.junit.Test;

public class ImpostoRendaTest {
	
	@Test
	public void deveriaSerIsentoAte1903Teste1() {
		ImpostoRenda calcImpostoSalario = new ImpostoRenda();
		double aliquota = calcImpostoSalario.calcImposto(1000.00); 
		assertEquals(1000.00, aliquota, 0.0001);
	}
	
	@Test
	public void deveriaSerIsentoAte1903Test2() {
		ImpostoRenda calcImpostoSalario = new ImpostoRenda();
		double aliquota = calcImpostoSalario.calcImposto(1903.98); 
		assertEquals(1903.98, aliquota, 0.0001);
	}
	
	
	@Test
	public void deveriaSerSeteEMeioEntre190399E282665Teste1() {
		ImpostoRenda calcImpostoSalario = new ImpostoRenda();
		double aliquota = calcImpostoSalario.calcImposto(1903.99); 
		assertEquals(1903.98925, aliquota, 0.0001);
	}
	
	@Test
	public void deveriaSerSeteEMeioEntre190399E282665Teste2() {
		ImpostoRenda calcImpostoSalario = new ImpostoRenda();
		double aliquota = calcImpostoSalario.calcImposto(2826.65); 
		assertEquals(2757.44975, aliquota, 0.0001);
	}
	
	@Test
	public void deveriaSerSeteEMeioEntre190399E282665Teste3() {
		ImpostoRenda calcImpostoSalario = new ImpostoRenda();
		double aliquota = calcImpostoSalario.calcImposto(2500.00); 
		assertEquals(2455.2985, aliquota, 0.0001);
	}
	
	
	@Test
	public void deveriaSerQuinzeEntre282666E375105Teste1() {
		ImpostoRenda calcImpostoSalario = new ImpostoRenda();
		double aliquota = calcImpostoSalario.calcImposto(2826.66); 
		assertEquals(2757.45849, aliquota, 0.0001);
	}
	
	@Test
	public void deveriaSerQuinzeEntre282666E375105Teste2() {
		ImpostoRenda calcImpostoSalario = new ImpostoRenda();
		double aliquota = calcImpostoSalario.calcImposto(3751.05); 
		assertEquals(3543.19, aliquota, 0.0001);
	}
	
	@Test
	public void deveriaSerQuinzeEntre282666E375105Teste3() {
		ImpostoRenda calcImpostoSalario = new ImpostoRenda();
		double aliquota = calcImpostoSalario.calcImposto(3300.66);
		assertEquals(3160.3585, aliquota, 0.0001);
	}
	
	
	@Test
	public void deveriaSerVinteEDoisEMeioEntre375106E466468Teste1() {
		ImpostoRenda calcImpostoSalario = new ImpostoRenda();
		double aliquota = calcImpostoSalario.calcImposto(3751.06); 
		assertEquals(3543.19775, aliquota, 0.0001);
	}
	
	@Test
	public void deveriaSerVinteEDoisEMeioEntre375106E466468Teste2() {
		ImpostoRenda calcImpostoSalario = new ImpostoRenda();
		double aliquota = calcImpostoSalario.calcImposto(4664.68); 
		assertEquals(4251.25325, aliquota, 0.0001);
	}
	
	@Test
	public void deveriaSerVinteEDoisEMeioEntre375106E466468Teste3() {
		ImpostoRenda calcImpostoSalario = new ImpostoRenda();
		double aliquota = calcImpostoSalario.calcImposto(4200.56); 
		assertEquals(3891.56025, aliquota, 0.0001);
	}
	
	
	@Test
	public void deveriaSerVinteESeteEMeioAcimaDe466468Teste1() {
		ImpostoRenda calcImpostoSalario = new ImpostoRenda();
		double aliquota = calcImpostoSalario.calcImposto(4664.69); 
		assertEquals(4251.25725, aliquota, 0.0001);
	}
	
	@Test
	public void deveriaSerVinteESeteEMeioAcimaDe466468Teste2() {
		ImpostoRenda calcImpostoSalario = new ImpostoRenda();
		double aliquota = calcImpostoSalario.calcImposto(6000.68); 
		assertEquals(5219.85, aliquota, 0.0001);
	}
	
	@Test
	public void deveriaSerVinteESeteEMeioAcimaDe466468Teste3() {
		ImpostoRenda calcImpostoSalario = new ImpostoRenda();
		double aliquota = calcImpostoSalario.calcImposto(8000.69); 
		assertEquals(6669.85725, aliquota, 0.0001);
	}

}
